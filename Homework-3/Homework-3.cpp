#include <iostream>
#include "Vector.h"


void printVector(Vector& vector)
{
	std::cout << std::endl << "size: " << vector.size() << std::endl;

	for (int i = 0; i < vector.size(); i++)
	{
		std::cout << vector[i] << std::endl;
	}
}


int main()
{
	Vector vector_a(5);
	vector_a.push_back(3);
	vector_a.push_back(5);
	vector_a.push_back(8);
	vector_a.push_back(3);


	Vector vector_b(3);
	vector_b.push_back(2);
	vector_b.push_back(7);
	vector_b.push_back(2);
	vector_b.push_back(4);

	std::cout << vector_a;
	std::cout << vector_b;
}

